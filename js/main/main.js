var DSNV = "DSNV";
var dsnv = [];
var dsnvJson = localStorage.getItem(DSNV);

// convert json to array
if (dsnvJson != null) {
  var nvArr = JSON.parse(dsnvJson);
  // convert array chứa object không có key tinhDTB() thành array chứa object có key tinhDTB()
  dsnv = nvArr.map(function(item) {
    return new thongtin(
        item.taikhoan,
        item.hoten,
        item.email,
        item.matkhau,
        item.ngaylam,
        item.luong,
        item.chucvu,
        item.giolam
      );
  });
  renderDSNV(dsnv);
}
function themnhanvien() {
    var nv = layThongTinTuForm();
    var isValid = true;
    isValid = kiemtradodai(nv.taikhoan,"tbTKNV", 4, 6);
    isValid = isValid & kiemtraTen(nv.hoten) & kiemtradodai(nv.luong, "tbLuongCB", 1000000, 10000000) & kiemtraEmail(nv.email) & kiemtraMatkhau(nv.matkhau) & kiemtradodai(nv.giolam, "tbGiolam", 80, 200);
    if(isValid) {
       dsnv.push(nv);
        var dssvJson = JSON.stringify(dsnv);
        localStorage.setItem(DSNV, dssvJson);
        renderDSNV(dsnv); 
    }
      
}

function xoaNhanvien(idNv) {
    var vitri = timkiem(idNv, dsnv);
    if(vitri != -1) {
        dsnv.splice(vitri, 1);
        renderDSNV(dsnv);
    }
}

function suaNhanvien(idNv) {
    var vitri = timkiem(idNv, dsnv);
    if(vitri == -1) {
      return;
    }
    var nv = dsnv[vitri];
    showthontintuForm(nv); 
}

function capnhatthongtin() {
  var nv = layThongTinTuForm();
  var vitri = timkiem(nv.taikhoan, dsnv);
  if(vitri != -1) {
    dsnv[vitri] = nv;
    renderDSNV(dsnv);
  }
    console.log("🚀 ~ file: main.js:61 ~ capnhatthongtin ~ dsnv", dsnv)

}