function kiemtradodai(value, idErr, min, max) {
    var length = value.length;
    if(length < min || length > max) {
        document.getElementById(idErr).innerText = `Độ dài phải từ ${min} đến ${max} kí tự.`;
        console.log(length);
        return false;
    } else {
        document.getElementById(idErr).innerText = "";
        return true;
    }
}

function kiemtraTen(value) {
    var reg = /^[a-zA-Z ]+$/;
    var sTring = reg.test(value);
    if(sTring) {
        document.getElementById("tbTen").innerText = "";
        return true;
    } else {
        document.getElementById("tbTen").innerText = "Trường này chỉ được nhập chữ !!!";
        return false;
    }
}

function kiemtraEmail(value) {
    const reg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var isemail = reg.test(value);
    if(isemail) {
        document.getElementById("tbEmail").innerText = "";
        return true;
    } else {
        document.getElementById("tbEmail").innerText = "Email không hợp lệ !!!";
        return false;
    }
}

function kiemtraMatkhau(value) {
    const reg = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{8,}$/;
    var matkhau = reg.test(value)
    if(matkhau) {
        document.getElementById("tbMatKhau").innerText = "";
        return true;
    } else {
        document.getElementById("tbMatKhau").innerText = "Mật khẩu không hợp lệ !!!";
        return false;
    }

}